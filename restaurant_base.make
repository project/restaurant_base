core = 7.x
api  = 2

; Dependencies

projects[field_collection][version] = 1.0-beta8
projects[field_collection][subdir] = contrib

projects[navbar][version] = 1.x-dev
projects[navbar][subdir] = contrib
projects[navbar][download][type] = git
projects[navbar][download][revision] = 455f81d
projects[navbar][download][branch] = 7.x-1.x
projects[navbar][patch][1757466] = http://drupal.org/files/navbar-conflict-1757466-14.patch
projects[navbar][patch][2050559] = http://drupal.org/files/z-index-heart-cools-2050559-1.patch
projects[navbar][patch][2595781] = https://www.drupal.org/files/issues/navbar-change-default-orientation-to-vertical-2595781-1.patch

projects[fences][version] = 1.2
projects[fences][subdir] = contrib

projects[eck][version] = 2.0-rc7
projects[eck][subdir] = contrib

projects[variable][version] = 2.1
projects[variable][subdir] = contrib

projects[migrate][version] = 2.5
projects[migrate][subdir] = contrib

projects[migrate_extras][version] = 2.5
projects[migrate_extras][subdir] = contrib

projects[view_mode_templates][version] = 1.0
projects[view_mode_templates][subdir] = contrib

projects[smart_trim][version] = 1.5
projects[smart_trim][subdir] = contrib

projects[email][version] = 1.2
projects[email][subdir] = contrib

projects[auto_nodetitle][version] = 1.0
projects[auto_nodetitle][subdir] = contrib

projects[addressfield][version] = 1.0-beta5
projects[addressfield][subdir] = contrib

projects[disqus][version] = 1.10
projects[disqus][subdir] = contrib

projects[better_exposed_filters][version] = 3.0-beta4
projects[better_exposed_filters][subdir] = contrib

projects[calendar][version] = 3.4
projects[calendar][subdir] = contrib
projects[calendar][patch][1471400] = https://www.drupal.org/files/issues/calendar-illegal_offset-1471400-78.patch

projects[entity][version] = 1.6
projects[entity][subdir] = contrib

projects[google_analytics][version] = 1.4
projects[google_analytics][subdir] = contrib

projects[computed_field][version] = 1.0
projects[computed_field][subdir] = contrib

projects[custom_add_another][version] = 1.0-rc3
projects[custom_add_another][subdir] = contrib

projects[better_exposed_filters][version] = 3.0-beta4
projects[better_exposed_filters][subdir] = contrib

projects[features_override][version] = 2.0-rc2
projects[features_override][subdir] = contrib

projects[entity_view_mode][version] = 1.0-rc1
projects[entity_view_mode][subdir] = contrib

projects[smart_trim][version] = 1.5
projects[smart_trim][subdir] = contrib

projects[page_title][version] = 2.7
projects[page_title][subdir] = contrib
projects[page_title][patch][2516726] = https://www.drupal.org/files/issues/page_title-fixes_redeclaration_bug_on_repeated_includes-2516726-1.patch

projects[eck][version] = 2.0-rc7
projects[eck][subdir] = contrib

projects[inline_entity_form][version] = 1.6
projects[inline_entity_form][subdir] = contrib

projects[commerce][version] = 1.11
projects[commerce][subdir] = contrib
projects[commerce][patch][2567833] = https://www.drupal.org/files/issues/commerce-fatal-error-commerce_product_ui.module-2567833-1.patch

projects[commerce_backoffice][version] = 1.4
projects[commerce_backoffice][subdir] = contrib

projects[commerce_features][version] = 1.1
projects[commerce_features][subdir] = contrib

projects[rules][version] = 2.9
projects[rules][subdir] = contrib

projects[entityqueue][version] = 1.1
projects[entityqueue][subdir] = contrib

projects[auto_entityqueue][version] = 1.x-dev
projects[auto_entityqueue][subdir] = contrib

projects[addressfield_staticmap][version] = 1.0
projects[addressfield_staticmap][subdir] = contrib

projects[uuid_features][version] = 1.x-dev
projects[uuid_features][subdir] = contrib

projects[search_autocomplete][version] = 4.7
projects[search_autocomplete][subdir] = contrib

projects[radix_core][version] = 3.x-dev
projects[radix_core][subdir] = contrib

projects[radix_views][version] = 1.0
projects[radix_views][subdir] = contrib

projects[panels_everywhere][version] = 1.0-rc2
projects[panels_everywhere][subdir] = contrib

projects[imagecache_token][version] = 1.0-rc1
projects[imagecache_token][subdir] = contrib
